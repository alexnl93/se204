//-----------------------------------------------------------------
// Wishbone BlockRAM
//-----------------------------------------------------------------
//
// Le paramètre mem_adr_width doit permettre de déterminer le nombre 
// de mots de la mémoire : (2048 pour mem_adr_width=11)

module wb_bram #(parameter mem_adr_width = 11) (
						// Wishbone interface
						wshb_if.slave wb_s
						);

   reg   [3:0][7:0] mem [(2**mem_adr_width)-1:0];
   logic 	ack_r, read, write,in_burst;
   logic  [mem_adr_width-1:0] address;

   assign wb_s.err = 0;
   assign wb_s.rty = 0;

   //stb (strobe à 1 seulement quand cyc est à 1
   assign write = wb_s.stb && wb_s.we ;
   assign read = wb_s.stb && (!(wb_s.we)) ;
   assign wb_s.ack = write || ack_r ;
   assign address = in_burst ? wb_s.adr[mem_adr_width-1+2:2]+1 : wb_s.adr[mem_adr_width-1+2:2];
  
//block mémoire 
   always_ff@(posedge wb_s.clk)
     begin
	wb_s.dat_sm <= mem[address];
	if (write)
	  begin
	     if (wb_s.sel[0])
	       mem[address][0] <= wb_s.dat_ms[7:0];
	     if (wb_s.sel[1])
	       mem[address][1] <= wb_s.dat_ms[15:8];
	     if (wb_s.sel[2])
	       mem[address][2] <= wb_s.dat_ms[23:16];
	     if (wb_s.sel[3])
	       mem[address][3] <= wb_s.dat_ms[31:24];
	  end	
     end

    
   always_ff@(posedge wb_s.clk or posedge wb_s.rst)
     if (wb_s.rst)
       in_burst <= 0;
     else
       if (read)
	 if (wb_s.cti ==  3'b010)
	     in_burst <= 1;
	 else
	   in_burst <= 0;
   
   always_ff@(posedge wb_s.clk or posedge wb_s.rst)
     begin
	if (wb_s.rst)
	  ack_r <= 0;
	else
	  case(wb_s.cti)
	    3'b000:
	      begin
		 if (ack_r)
		   ack_r <= 0;
		 else
		   if(read)
		     ack_r <= 1;
		   else
		     ack_r <= 0;
	      end
	    3'b010:
	      begin
		 if(read)
		   ack_r <= 1;
	      end
	    3'b111:
	      ack_r <= 0;
	  endcase
     end
   
   
endmodule

