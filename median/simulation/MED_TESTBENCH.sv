/**
 * \file      MED_TESTBENCH.sv
 * \author    Alexnl022
 * \version   1.0
 * \date      7 mars 2016 
 * \brief     Module test de MED
 */

module MED_TESTBENCH();

   parameter NB_OF_PIXELS = 9;

   //Wire med
   logic [7:0] DI;
   logic       DSI;
   logic       BYP;
   logic       CLK;
   logic [7:0] DO;

   //Internal Datta
   logic [7:0] V [8:0];
   logic [4:0] i,pos,pix_idx;
   logic [7:0] MEDIANE,BUFFER;
   
   MED med(.DI(DI),
	   .DSI(DSI),
	   .BYP(BYP),
	   .CLK(CLK),
	   .DO(DO));   

   /**
    *\brief Test du remplissage des registres et du filtre med
    */
   
   initial
     begin
	//initialisation
	CLK = 1;
	BYP = 0;
	DSI = 0;
	DI = 0;
	i = 0;
	
	/**
	 *\brief Test du remplissage des registres
	 */
	
	//BYP et DSIà mettre à 1 pendant l'ajout de data
	
	repeat(100)
 	  begin
	     CLK = ~CLK;
	     #10ns;
	     CLK = ~CLK;
	     DSI = 1;
	     BYP = 1;
	     repeat(NB_OF_PIXELS)
	       begin
		  #10ns;
		  //période de 20ns
		  CLK = ~CLK;
		  DI = $random;

		  //On enregistre les valeurs dans un tableau
		  //pour trouver la valeur mediane différement
		  //et la comparer
		  V[i] = DI;
		  if (i<8)
		    i=i+1;
		  else
		    i=0;
		  
		  #10ns;
		  CLK = ~CLK; 
	       end

	     /**
	      *\brief Filtre median
	      */

	     //Calcul de la valeur mediane en software
	     //Tri des valeurs et assignation du median
	     for (pos = 0; pos <= NB_OF_PIXELS-2;pos++)
	       begin
		  for (pix_idx = pos+1; pix_idx <= NB_OF_PIXELS-1; pix_idx++)
		    begin
		       if (V[pos] < V[pix_idx])
			 begin
			    // Echange de V[pos] et V[pix_idx]
			    BUFFER = V[pos];
			    V[pos] = V[pix_idx];
			    V[pix_idx] = BUFFER;
			 end
		    end
		  MEDIANE = V[NB_OF_PIXELS/2];
	       end

	     
	     //Calcul de la valeur mediane en hardware
	     for(pos = 1; pos <= (NB_OF_PIXELS/2); pos++)
	       begin
		  repeat(NB_OF_PIXELS - pos)
		    begin
		       #10ns;	
		       CLK = ~CLK;
		       BYP = 0;
		       //A ne mettre qu'une seule fois à zero
		       DSI = 0;
		       #10ns;
		       CLK = ~CLK;
		    end

		  repeat(pos)
		    begin
		       #10ns;	
		       CLK = ~CLK;
		       BYP = 1;
		       #10ns;
		       CLK = ~CLK;
		    end
	       end 

	     repeat(pos)
	       begin
		  #10ns;	
		  CLK = ~CLK;
		  BYP = 0;
		  #10ns;
		  CLK = ~CLK;
	       end 

	     //Conclusion
	     if (DO != MEDIANE)
	       begin
		  $display("BAD RESULT!");
		  $display("Current Mediane: %d",DO);
		  $display("Expected Mediane: %d",MEDIANE);
		  $display("For the series :");
		  for(i=0;i<9;i++)
		    $display("V[%d]: %d",i,V[i]);
		  $stop;
	       end
	     
	  end // repeat (100)
	$finish;
     end // initial begin

endmodule

   
