/**
 * \file      MCE_TESTBENCH.sv
 * \author    Alexnl022
 * \version   1.0
 * \date      3 mars 2016 
 * \brief     Module test de MCE
 */

module MCE_TESTBENCH();

   logic [7:0] A,B,MAX,MIN;

   MCE mce(.A(A),
	   .B(B),
	   .MIN(MIN),
	   .MAX(MAX));
   
   initial
     begin
	repeat(1000)
	  begin
	     A = $random;
	     B = $random;
	     #10ns;
	     
	     //On vérifie que MAX est supérieur ou égale à MIN
	     if (MAX < MIN)
	       begin
		  $display("simuation failed!");
		  $display("MIN is bigger than MAX");
		  if (A < B)
		    begin
		       $display("expected MIN: %d, current MIN %d",A,MIN);
		       $display("expected MAX: %d, current MAX %d",B,MAX);	
		    end
		  if (B < A)
		    begin
		       $display("expected MIN: %d, current MIN %d",B,MIN);
		       $display("expected MAX: %d, current MAX %d",A,MAX);	
		    end
		  $stop;
	       end
	     
  
	     //On vérifie que l'on ait bien la bonne valeur pour MIN
	     //On vérifie que l'on ait bien la bonne valeur pour MAX	     
	     if (A < B)
		  if ((A != MIN) || (B != MAX))
		      begin
		      $display("simuation failed!");		      
		      $display("expected MIN: %d, current MIN %d",A,MIN);
		      $display("expected MAX: %d, current MAX %d",B,MAX);
		      $stop;
		      end
	     if (B < A)
	        if ((B != MIN) || (A != MAX))
		      begin
		      $display("simuation failed!");		      
		      $display("expected MIN: %d, current MIN %d",B,MIN);
		      $display("expected MAX: %d, current MAX %d",A,MAX);
		      $stop;
		      end
	     
	  end
	$finish;
     end // initial begin

endmodule

   
