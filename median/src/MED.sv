/**
 * \file      MED.sv
 * \author    Alexnl022
 * \version   1.0
 * \date      7 mars 2016 
 * \brief     Filtre median
 * \brief     9 pixels par défaut
 * \brief     8 bits par pixels par defaut 
 */

module MED #(parameter SIZE_PIXEL = 8, NB_OF_PIXELS = 9)
   (
    //Entrees
    input [SIZE_PIXEL-1:0]  DI,
    input 		    DSI,
    input 		    BYP,
    input 		    CLK,
   
    //Sorties
    output [SIZE_PIXEL-1:0] DO
    );

   
   reg [NB_OF_PIXELS-1:0] [SIZE_PIXEL-1:0] R;
   wire [SIZE_PIXEL-1:0] 		   MAX;
   wire [SIZE_PIXEL-1:0] 		   MIN;

   /**
    *\brief Branchement au module MCE
    */
 
   MCE mce(.A(R[8]),
	   .B(R[7]),
	   .MIN(MIN),
	   .MAX(MAX));

   assign DO = R[8];
   
   
   /**
    *\brief Remplissage du tableau de pixel
    *\brief Mise en place de la sortie DO
    */
   
   always_ff@(posedge CLK)
     begin
	//Correspond au premier MUX cf schéma
	if (DSI == 1)
	  R[NB_OF_PIXELS-2:0] <= {R[NB_OF_PIXELS-3:0],DI};
	else
	  R[NB_OF_PIXELS-2:0] <= {R[NB_OF_PIXELS-3:0],MIN};
	//Correspond au dernier MUX cf schéma
	if (BYP == 1)
	  R[8] <= R[7];
	else
	  R[8] <= MAX;	
     end
   
endmodule // MED
