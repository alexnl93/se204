/**
 * \file      MEDIAN.sv
 * \author    Alexnl022
 * \version   1.0
 * \date      9 mars 2016 
 * \brief     Operateur complet filtre median
 * \brief     9 pixels par uniquement 
 */

module MEDIAN #(parameter SIZE_PIXEL = 8, NB_OF_PIXELS = 9)
   (
    //Entrees
    input [SIZE_PIXEL-1:0]  DI,
    input 		    DSI,
    input 		    nRST, 
    input 		    CLK,
   
    //Sorties
    output [SIZE_PIXEL-1:0] DO,
    output reg 		    DSO		    
    );

   logic 		    BYP;
   
   /**
    *\brief Branchement au module MED
    */
   
   MED med(.DI(DI),
	   .DSI(DSI),
	   .BYP(BYP),
	   .CLK(CLK),
	   .DO(DO));

   logic [4:0] 		    periode;
   logic [4:0] 		    etape;   

   always_comb
     if(DSI)
       begin
	  BYP = 1;
	  DSO = 0;
       end
     else
       begin
	  BYP = (periode >= NB_OF_PIXELS - etape);
	  DSO = ((etape > (NB_OF_PIXELS/2)) && (periode > (NB_OF_PIXELS/2-1)));
       end

   
   always_ff@(posedge CLK or negedge nRST)
     if(!nRST) // reset asynchrone
       begin
	  etape <= 1;
	  periode <= 0;
       end
     else
       // pendant le chargement 
       if(DSI)
	 begin
	    etape <= 1;
	    periode <= 0;
	 end
       else
	 begin
	    if (!DSO)
	      begin
		 periode <= periode + 1;
		 if(periode == NB_OF_PIXELS - 1)
		   begin
		      periode <= 0;
		      etape <= etape + 1;
		   end
	      end
	 end // else: !if(DSI)
   
endmodule // MEDIAN
