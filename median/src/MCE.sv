/**
 * \file      MCE.sv
 * \author    Alexnl022
 * \version   1.0
 * \date      3 mars 2016 
 * \brief     Comparateur de nombre
 */

module MCE #(parameter SIZE = 8)
   (
    //Entrees
    input [SIZE-1:0]  A,
    input [SIZE-1:0]  B,
   
    //Sorties
    output [SIZE-1:0] MAX, 
    output [SIZE-1:0] MIN	    
    );

   /**
    *\brief Utilisation des assign seulement
    */
   assign MAX = (A > B) ? A : B;
   assign MIN = (A > B) ? B : A;
   
endmodule 
