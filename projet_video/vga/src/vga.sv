module vga #(parameter HDISP = 640, VDISP = 480)(
	    input clk,
	    input nrst,
	    vga_if.master vga_ifm,
	    wshb_if.master wshb_ifm				 
	    );

   
   localparam HFP = 16, HPULSE = 96, HBP = 48, VFP = 11, VPULSE = 2, VBP = 31;
   localparam SIZE_LINE = VDISP + VFP + VPULSE + VBP;
   localparam SIZE_COLUMN = HDISP + HFP + HPULSE + HBP;
   localparam SIZE_COUNTER_LINE = $clog2 (SIZE_LINE);
   localparam SIZE_COUNTER_COLUMN = $clog2 (SIZE_COLUMN);

   logic 	  locked;
   logic 	  rst;
   logic 	  vga_clk;
   logic 	  test_empty;
   logic         enable_read;
   logic 	  read;
   logic 	  wfull;	  
   logic [SIZE_COUNTER_LINE-1:0] counter_line;
   logic [SIZE_COUNTER_COLUMN-1:0] counter_column;
   //registre connecté à la mémoire
   logic [15:0] 		   buffer_pixel;

   vga_pll vga_pll1(.refclk(clk),
                    .rst(!nrst),
                    .outclk_0(vga_clk),
                    .locked(locked)
                    );

   fpga_reset #(1) vga_reset(.fpga_NRST(nrst),
                             .fpga_CLK(vga_clk),
                             .n_rst(rst)
                             );

   
   fifo_async #(16,8) fifo_async(.rst(rst),
				 .rclk(vga_clk),
				 .read(read), //combinaison avec blank et full une fois
				 .rdata(buffer_pixel),
				 .rempty(test_empty),
				 .wclk(wshb_ifm.clk),
				 .wdata(wshb_ifm.dat_sm),
				 .write(wshb_ifm.ack),
				 .wfull(wfull)
				 );

   assign wshb_ifm.stb = ~wfull; //dans le même domaine d'horloge
   assign wshb_ifm.cyc =	wshb_ifm.stb;
   assign wshb_ifm.cti =	'0;
   assign wshb_ifm.bte =	'0;
   assign wshb_ifm.we = 1'b0;
   assign wshb_ifm.dat_ms = 16'hFFFF;
   assign wshb_ifm.sel =	2'b11;

   assign vga_ifm.VGA_SYNC = 0;
   assign vga_ifm.VGA_CLK = ~vga_clk;
   assign vga_ifm.VGA_R = {buffer_pixel[15:11],3'b0};
   assign vga_ifm.VGA_G = {buffer_pixel[10:5],2'b0};
   assign vga_ifm.VGA_B = {buffer_pixel[4:0],3'b0};

   //bloc de test pour debuguer
   //comprendre si la couleur est a bonne
   logic [1:0] 			   test;
   assign test = wshb_ifm.adr[4:3];
   
   logic 			   cVGA_HS   ;
   logic 			   cVGA_VS   ;
   logic 			   cVGA_BLANK;

   always_comb
     begin
	cVGA_HS    = (counter_column < HDISP + HFP)|| (counter_column >= HDISP
						       + HFP + HPULSE);
	cVGA_VS    = (counter_line < VDISP + VFP)  || (counter_line >= VDISP +
						       VFP + VPULSE);
	cVGA_BLANK = (counter_column < HDISP)      && (counter_line < VDISP);
     end

   always_ff@(posedge vga_clk)
     begin
	vga_ifm.VGA_HS    <= cVGA_HS;
	vga_ifm.VGA_VS    <= cVGA_VS;
	vga_ifm.VGA_BLANK <= cVGA_BLANK;
     end
   
   assign read = cVGA_BLANK && enable_read;
   
   //gestion du compteur colonne  
   always_ff@(posedge vga_clk or posedge rst) 
     if (rst)
	  counter_column <= 0;
     else
       if (counter_column == (SIZE_COLUMN - 1))
	 counter_column <= 0;
       else
	 counter_column <= counter_column + 1'b1;
   
   //gestion du compteur ligne
   always_ff@(posedge vga_clk or posedge rst) 
     if (rst)
       counter_line <= 0;
     else
       if (counter_column == (SIZE_COLUMN - 1))
	 if (counter_line == (SIZE_LINE-1))
	   counter_line <= 0;
	 else
	   counter_line <= counter_line + 1'b1;

   //on incrémente l'addresse de lecture des données dans la fifo
   always_ff@(posedge wshb_ifm.clk  or posedge wshb_ifm.rst)
     if (wshb_ifm.rst)
       wshb_ifm.adr <= 0;
     else
       if(wshb_ifm.ack && (!wshb_ifm.we))
	 if (wshb_ifm.adr == (HDISP*VDISP-1)*2)
	   wshb_ifm.adr <= 0;
	 else
	   wshb_ifm.adr <= wshb_ifm.adr + 2;

   logic [1:0] _fifo_full;
   logic       fifo_first_full;
   
   always_ff@(posedge vga_clk or posedge rst) 
     if (rst)
       begin
          _fifo_full     <= '0;
	  enable_read     <= 0;
	  fifo_first_full <= '0;
       end
     else
       begin
          // resync. wfull car venant de wb
          _fifo_full <= {_fifo_full[0],wfull};
          // La première fois que la fifo est pleine
          if (_fifo_full[1])
            fifo_first_full <= 'd1;
          // La fifo est pleine et on a fini une image on est dans la  zone non affichée
            if (fifo_first_full && (counter_line >= VDISP))
              enable_read <= 1;
     end
endmodule // vga





