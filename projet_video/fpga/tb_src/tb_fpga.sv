module tb_fpga ();

   localparam HDISP = 160, VDISP = 90;

   logic  fpga_CLK;
   logic  fpga_CLK_AUX;
   logic  fpga_LEDR0;
   logic  fpga_LEDR1;
   logic  fpga_LEDR2;
   logic  fpga_LEDR3;
   logic  fpga_SW0;
   logic  fpga_SW1;
   logic  fpga_NRST;
   logic  fpga_SEL_CLK_AUX;
   vga_if vga_if0() ;
   sdram_if sdram_if0();

   fpga  #(HDISP,VDISP) fpga_1(     .fpga_CLK(fpga_CLK),
				    .fpga_CLK_AUX(fpga_CLK_AUX),
				    .fpga_LEDR0(fpga_LEDR0),
				    .fpga_LEDR1(fpga_LEDR1),
				    .fpga_LEDR2(fpga_LEDR2),
				    .fpga_LEDR3(fpga_LEDR3),
				    .fpga_SW0(fpga_SW0),
				    .fpga_SW1(fpga_SW1),
				    .fpga_NRST(fpga_NRST),
				    .fpga_SEL_CLK_AUX(fpga_SEL_CLK_AUX),
				    .vga_ifm(vga_if0),
				    .sdram_ifm(sdram_if0)
		    );

   
   screen #(.mode(0),.X(HDISP),.Y(VDISP)) screen0(.vga_ifs(vga_if0));

   sdr #(.Debug(0)) SDRAM
  (
                  .Clk    (sdram_if0.clk     ),
                  .Cke    (sdram_if0.cke     ),
                  .Cs_n   (sdram_if0.cs_n    ),
                  .Ras_n  (sdram_if0.ras_n   ),
                  .Cas_n  (sdram_if0.cas_n   ),
                  .We_n   (sdram_if0.we_n    ),
                  .Addr   (sdram_if0.sAddr   ),
                  .Ba     (sdram_if0.ba      ),
                  .Dq     (sdram_if0.sDQ     ),
                  .Dqm    (sdram_if0.dqm     )
 ) ;
   
   always #10ns fpga_CLK = ~fpga_CLK;

   always #18.52ns if (fpga_SEL_CLK_AUX) fpga_CLK_AUX = ~fpga_CLK_AUX;
   
   initial begin: ENTREES
      
      fpga_SW0     = 0;
      fpga_SW1     = 1;
      fpga_CLK     = 0;
      fpga_CLK_AUX = 0;
      fpga_NRST    = 0;

      #1us;
      // remove the reset
      fpga_NRST = 1;
      #1ns;
      fpga_NRST = 0;
      #1ns;
      fpga_NRST = 1; 
      #300ns
	fpga_SW0 = 0;   
      #300ns
	fpga_SW0 = 1;
      #100ns
	fpga_SW0 = 0;
      #600ns
	fpga_SW0 = 1;
      #200ns
	fpga_SW0 = 0;    
      #0.008s;
      $stop;
      
   end
   
endmodule // tb_fpga
