module wshb_itercon (
		     wshb_if.slave wshb_ifs_mire,
		     wshb_if.slave wshb_ifs_vga,
		     wshb_if.master wshb_ifm
		     );

   logic 		   occuped_by_vga;

   assign wshb_ifs_mire.dat_sm = wshb_ifm.dat_sm;
   assign wshb_ifs_mire.ack = wshb_ifm.ack;                                     
   assign wshb_ifs_mire.err = wshb_ifm.err;                                     
   assign wshb_ifs_mire.rty = wshb_ifm.rty;
   
   assign wshb_ifs_vga.dat_sm = wshb_ifm.dat_sm;
   assign wshb_ifs_vga.ack = (wshb_ifm.ack && (!wshb_ifm.we));
   assign wshb_ifs_vga.err = wshb_ifm.err;                                     
   assign wshb_ifs_vga.rty = wshb_ifm.rty;                                     

   assign wshb_ifm.dat_ms = occuped_by_vga ? wshb_ifs_vga.dat_ms : wshb_ifs_mire.dat_ms;
   assign wshb_ifm.adr = occuped_by_vga ? wshb_ifs_vga.adr : wshb_ifs_mire.adr ;
   assign wshb_ifm.cyc = occuped_by_vga ? wshb_ifs_vga.cyc : wshb_ifs_mire.cyc;
   assign wshb_ifm.sel = occuped_by_vga ? wshb_ifs_vga.sel : wshb_ifs_mire.sel;
   assign wshb_ifm.stb = occuped_by_vga ? wshb_ifs_vga.stb : wshb_ifs_mire.stb;
   assign wshb_ifm.we = occuped_by_vga ? wshb_ifs_vga.we : wshb_ifs_mire.we;
   assign wshb_ifm.cti = occuped_by_vga ? wshb_ifs_vga.cti : wshb_ifs_mire.cti;
   assign wshb_ifm.bte = occuped_by_vga ? wshb_ifs_vga.bte : wshb_ifs_mire.bte;

   /**************************************************
    *
    * 2 cas:
    * 1- Le VGA a la main
    *    -> S'il demande la main, il la garde
    *    -> S'il ne demande pas la main et que la mire la demande, on lui sonne
    * 2- Le VGA n'a pas la main
    *    -> Si la mire ne fait pas de requête, le VGA récupère la main
    *    -> Si La mire fait une requête et qu'elle a un accusé de reception (ACK), alors si le VGA demande la main il la récupère
    * 
    ***********************************************/
   
   always_ff@(posedge wshb_ifm.clk or posedge wshb_ifm.rst)
     if (wshb_ifm.rst)
       occuped_by_vga <= 1;
     else
       case(occuped_by_vga)
	 1'b1:begin
	    if(!wshb_ifs_vga.stb && wshb_ifs_mire.stb)
	      occuped_by_vga <= 0;
	 end
	 1'b0:begin
	    if(!wshb_ifs_mire.stb)
	      occuped_by_vga <= 1; 
	    else
	      if(wshb_ifs_mire.ack && wshb_ifs_vga.stb)
		occuped_by_vga <= 1;
	 end
       endcase
   
endmodule


