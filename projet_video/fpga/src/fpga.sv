module fpga #(parameter HDISP = 640, VDISP = 480)(
						  // Partie hps 
`include "HPS_IO.svh"
						  input wire   fpga_CLK,
						  input wire   fpga_CLK_AUX,
						  output logic fpga_LEDR0,
						  output logic fpga_LEDR1,
						  output logic fpga_LEDR2,
						  output logic fpga_LEDR3,
						  input wire   fpga_SW0,
						  input wire   fpga_SW1,
						  input wire   fpga_NRST,
						  output logic fpga_SEL_CLK_AUX,
						  vga_if.master vga_ifm,
						  // Partie sdram
						  sdram_if.master sdram_ifm
						  );
   
`ifdef SIMULATION
   localparam COUNTER_SIZE=4 ;
`else
   localparam COUNTER_SIZE=26 ;
`endif
   
   logic [COUNTER_SIZE-1:0]     counter;
   logic [COUNTER_SIZE-2:0]     counter_aux;
   logic 			n_rst;

   // Instanciation de la PLL pour générer la clock wishbone et la clock sdram
   logic 			wshb_clk ;
   logic 			wshb_rst ;
   logic 			sdram_clk;
   logic 			locked ; 

   wshb_pll pll(
		.refclk(fpga_CLK),
		.rst(!fpga_NRST),
		.outclk_0(wshb_clk),
		.outclk_1(sdram_clk),
		.locked(locked));

   fpga_reset #(1) wshb_reset(.fpga_NRST(fpga_NRST),
			     .fpga_CLK(wshb_clk),
			     .n_rst(wshb_rst)
			     );

   // Instanciation d'un bush Wishbone 16 bits 
   wshb_if #(.DATA_BYTES(2)) wshb_if_0(wshb_clk,wshb_rst);
   wshb_if #(.DATA_BYTES(2)) wshb_if_vga(wshb_clk,wshb_rst);
   wshb_if #(.DATA_BYTES(2)) wshb_if_mire(wshb_clk,wshb_rst);

   
   // Instanciation du controleur de sdram
   wb16_sdram16 u_sdram_ctrl
     (
      // Wishbone 16 bits slave interface
      .wb_s(wshb_if_0.slave),
      // SDRAM master interface
      .sdram_m(sdram_ifm),
      // SDRAM clock
      .sdram_clk(sdram_clk) 
      );

   fpga_reset fpga_reset_1(.fpga_NRST(fpga_NRST),
			   .fpga_CLK(fpga_CLK),
			   .n_rst(n_rst)
			   );

   vga #(HDISP,VDISP) vga1(.clk(fpga_CLK_AUX),
			   .nrst(fpga_NRST),
			   .vga_ifm(vga_ifm),
			   .wshb_ifm(wshb_if_vga));

   //   mire mire1(.wshb_ifm(wshb_if_mire.master));
   hps_block hps0(.wshb_ifm(wshb_if_mire),.*) ;
        
   wshb_itercon wshb_itercon1(
			      .wshb_ifs_mire(wshb_if_mire),
			      .wshb_ifs_vga(wshb_if_vga),
			      .wshb_ifm(wshb_if_0)
			      );
   
   assign fpga_LEDR0 = fpga_SW0;
   assign fpga_LEDR2 = counter[COUNTER_SIZE-1];
   assign fpga_LEDR1 = counter_aux[COUNTER_SIZE-2];
   assign fpga_LEDR3 = n_rst;
   assign fpga_SEL_CLK_AUX = fpga_SW1;
   
   always_ff@(posedge fpga_CLK or negedge n_rst)
     if(!n_rst)
       counter <= 0;
     else
       counter++;
   
   always_ff@(posedge fpga_CLK_AUX or negedge n_rst)
     if(!n_rst)
       counter_aux <= 0;
     else
       counter_aux++;

   
   
endmodule
