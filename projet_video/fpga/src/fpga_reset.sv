module fpga_reset #(parameter rst_estate = 0) (fpga_NRST, fpga_CLK, n_rst);
   input  wire fpga_CLK;
   input  wire fpga_NRST;
   output logic n_rst;

   logic [1:0] R;

   always @(posedge fpga_CLK or negedge fpga_NRST)
     if(!fpga_NRST)
        R <= rst_estate? 2'b11 : 2'b00;
     else
        R <= rst_estate? {R[0],1'b0}:{R[0],1'b1} ;

   assign n_rst = R[1];

endmodule
