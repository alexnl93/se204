module mire #(parameter HDISP = 640, VDISP = 480)(
	     wshb_if.master wshb_ifm				 
	     );

   logic [$clog2(HDISP)-1:0] 	   compteur_colonne;
   logic [$clog2(VDISP)-1:0] 	   compteur_line;

   //d'abord 8 pixel en noir
   //puis 8 en bleu
   //puis 8 en vert
   //puis 8 en rouge
   
   always_comb
     case (compteur_colonne[4:3])
       2'b00: wshb_ifm.dat_ms = 16'h0000;
       2'b01: wshb_ifm.dat_ms = 16'h001F;
       2'b10: wshb_ifm.dat_ms = 16'h07E0;
       2'b11: wshb_ifm.dat_ms = 16'hF800;
     endcase // case (compteur_colonne[4:3])

   assign wshb_ifm.we = 1;
   assign wshb_ifm.cyc =	wshb_ifm.stb;
   assign wshb_ifm.cti =	'0;
   assign wshb_ifm.bte =	'0;
   assign wshb_ifm.sel =	2'b11;

   assign compteur_colonne = wshb_ifm.adr[6:1] % HDISP ;
   
      always_ff@(posedge wshb_ifm.clk or posedge wshb_ifm.rst)
     if (wshb_ifm.rst)
       begin
	  wshb_ifm.adr <= 0;
	  wshb_ifm.stb <= 1;	 
       end
     else
       if (wshb_ifm.ack)
	 begin
	    wshb_ifm.adr <= wshb_ifm.adr + 2;
	    if (wshb_ifm.adr[6:1] == 5'b11111)
	      wshb_ifm.stb <= 0;
	 end
       else
	 wshb_ifm.stb <= 1;
   
endmodule // mire


   
